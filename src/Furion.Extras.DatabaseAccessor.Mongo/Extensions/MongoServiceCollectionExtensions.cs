﻿using Furion.DatabaseAccessor;
using MongoDB;
using MongoDB.Driver;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// MongoDB 服务拓展类
    /// </summary>
    public static class MongoServiceCollectionExtensions
    {
        /// <summary>
        /// 添加 MongoDB 拓展
        /// </summary>
        /// <param name="services"></param>
        /// <param name="config"></param>
        /// <returns></returns>
        public static IServiceCollection AddMongoDB(this IServiceCollection services, MongoConnectionConfig config)
        {

            services.AddScoped<IMongoDatabase>(u =>
            {
                var client = new MongoClient(config.ConnectionString);
                var database = client.GetDatabase(config.DatabaseName);
                return database;
            });

            // 注册非泛型仓储
            services.AddScoped<IMongoDbRepository, MongoDbRepository>();

            // 注册 MongoDB 仓储
            services.AddScoped(typeof(IMongoDbRepository<>), typeof(MongoDbRepository<>));
            services.AddScoped(typeof(IMongoDbRepository<,>), typeof(MongoDbRepository<,>));

            return services;
        }
    }
}
