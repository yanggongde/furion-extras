# FurionExtras

#### 介绍
【非官方】基于Furion框架的第三方拓展库


#### FreeSql集成及使用
    安装拓展包 [Furion.Extras.DatabaseAccessor.FreeSqlEx](https://www.nuget.org/packages/Furion.Extras.DatabaseAccessor.FreeSqlEx)。
1.  注册 FreeSql 服务
    使用非常简单，只需要在 Startup.cs 中添加 services.AddFreeSql 即可。如：
```csharp
    services.AddFreeSql("Data Source=./Cps.db", FreeSql.DataType.Sqlite);
```
    同时也可以添加更多配置，如：
```csharp
    services.AddFreeSql("Data Source=./Cps.db", FreeSql.DataType.Sqlite, builder =>
    {
        builder.UseMonitorCommand(cmd => { }, (cmd, traceLog) =>
        {
            System.Console.WriteLine($"{cmd.CommandText}\r\n");
        });
    }, db =>
    {
        db.Aop.AuditValue += (s, e) =>
        {
            if (e.AuditValueType == AuditValueType.Insert)
            {
                
            }
            else if (e.AuditValueType == AuditValueType.Update)
            {
                
            }
        }
    });
```
2.  基本使用

    在使用之前，我们可以通过构造函数注入 IFreeSqlRepository<TEntity> 接口，如：
```csharp
    private readonly IFreeSqlRepository<Person> _freeSqlRepository;
    public PersonService(IFreeSqlRepository<Person> freeSqlRepository)
    {
        _freeSqlRepository = freeSqlRepository;
    }
```
2.1 新增操作
```csharp
    _freeSqlRepository.Insert(new Person(){ Name = "Furion" });
```
2.2 更新操作
```csharp
    _freeSqlRepository.Update(new Person(){ Id = 1, Name = "Furion/Fur" });
```
2.3 删除操作
```csharp
    _freeSqlRepository.Delete(1);
```
2.4 查询操作
```csharp
    var data = _freeSqlRepository.Where(u => u.Id > 10).ToList();
    
    var data  = _freeSqlRepository.Where(!string.IsNullOrEmpty(name), u => u.Name.Contains(name)).ToList();

    var data  = _freeSqlRepository.WhereIf(!string.IsNullOrEmpty(name), u => u.Name.Contains(name)).ToList();
```
2.5 分页查询
```csharp
    var data = _freeSqlRepository.Where(u => u.Id > 10).ToPagedList(1, 10);
```
3.  高级使用

3.1  IFreeSql 对象
```csharp
    var freeSql=_freeSqlRepository.Orm
```
    拿到这个对象之后，就可以使用 FreeSql 完整的功能，比如操作链表查询
```csharp
    var data = freeSql.Select<Order, OrderItem, Custom>()
                        .LeftJoin((o, i, c) => o.Id == i.OrderId)
                        .LeftJoin((o, i, c) => c.Id == o.CustomId)
                         .ToList((o,i,c)=> new ViewOrder
                         {
                             Id=o.Id,// o.*
                             CustomName=c.Name   //[c].[Name] AS [CustomName]
                         });
```
3.2  IAdo 对象
```csharp
    var ado = _freeSqlRepository.Ado;
```
    拿到该对象就可以操作原生 sql，如：
```csharp
    List<T> list = ado.Query<T>("select * from t1");
```
4.  关于仓储
    Furion.Extras.DatabaseAccessor.FreeSqlEx 框架为 FreeSql 提供了三个仓储：  
    IFreeSqlRepository：非泛型仓储，可以通过 .Change&lt;TEntity&gt; 方法切换到任何实体仓储  
    IFreeSqlRepository&lt;TEntity&gt;：实体泛型仓储  
    IFreeSqlRepository&lt;TEntity,TKey&gt;：实体泛型仓储
